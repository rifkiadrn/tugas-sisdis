package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"gopkg.in/yaml.v2"
)

func main() {
	port := os.Args[1]
	fmt.Println("Launching server on port: " + port)
	serverSocket, _ := net.Listen("tcp", ":"+port)

	defer func() {
		serverSocket.Close()
		fmt.Println("Server has stopped")
	}()
	for {
		conn, _ := serverSocket.Accept()
		myServer := NewHTTPServer(conn)

		go myServer.startAction()
	}

}

type HTTPServerInterface interface {
	startAction()
}
type HTTPServer struct {
	clientConn net.Conn
}

func NewHTTPServer(socketConn net.Conn) HTTPServerInterface {
	return &HTTPServer{
		clientConn: socketConn,
	}
}
func (s *HTTPServer) startAction() {
	reader := bufio.NewReader(s.clientConn)
	buf := make([]byte, 1024)

	n, _ := reader.Read(buf)
	// fmt.Println(string(buf[:n]))
	data := strings.Split(string(buf[:n]), "\r\n\r\n")
	header := make(map[string]string)
	for _, s := range strings.Split(data[0], "\n") {
		eachHeader := strings.Split(s, ": ")
		if len(eachHeader) == 2 {
			header[eachHeader[0]] = eachHeader[1]
		} else {
			header["firstHeader"] = eachHeader[0]
		}
	}
	body := ""
	if len(data) > 1 {
		body = data[1]
	}

	defer s.clientConn.Close()
	s.buildResponse(header, body)
}

func (s *HTTPServer) buildResponse(header map[string]string, body string) {
	currentVisit := time.Now()
	firstHeaderFragment := strings.Split(header["firstHeader"], " ")
	writer := bufio.NewWriter(s.clientConn)

	fmt.Printf("%s with endpoint %s and http version %s\n", firstHeaderFragment[0], firstHeaderFragment[1], firstHeaderFragment[2])
	if firstHeaderFragment[1] == "/" {
		found(writer, "/hello-world")
		writer.Flush()
		fmt.Println("Connection closed.")
		return
	}
	if firstHeaderFragment[1] == "/api/hello" && strings.ToUpper(firstHeaderFragment[0]) != "POST" {
		SendError(writer, "You cant use this url for the specified method", 405, "Method Not Allowed")
		writer.Flush()
		fmt.Println("Connection closed.")
		return
	}
	if strings.ToUpper(firstHeaderFragment[0]) == "GET" {
		fmt.Println(firstHeaderFragment[0])
		requestedFile := firstHeaderFragment[1]
		u, _ := url.Parse(requestedFile)
		m, _ := url.ParseQuery(u.RawQuery)

		if len(m) == 0 {
			if firstHeaderFragment[1] == "/hello-world" || firstHeaderFragment[1] == "/style" || firstHeaderFragment[1] == "/background" {
				contentType := getContentType(requestedFile)
				content := getFileContent(getFileName(requestedFile), "World")

				StatusOK(writer, contentType, content)
			} else if strings.HasPrefix(firstHeaderFragment[1], "/api/plusone") {
				val, err := strconv.Atoi(strings.TrimPrefix(firstHeaderFragment[1], "/api/plusone/"))
				if err != nil {
					SendError(writer, "The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.", 404, "Not Found")
				} else {
					SendPlusoneret(writer, val)
				}
			} else if firstHeaderFragment[1] == "/api/spesifikasi.yaml" {
				contentType := getContentType(requestedFile)
				content := getFileContent(getFileName(requestedFile), "")

				StatusOK(writer, contentType, content)
			} else {
				badRequest(writer, "endpoint not exist")
			}
		} else {
			info := ""
			if m["type"][0] == "time" {
				t := time.Now()
				info = fmt.Sprint(t.Format("Mon Jan _2 15:04:05 MST 2006"))
			} else if m["type"][0] == "random" {
				info = strconv.Itoa(rand.Int())
			} else {
				info = "No Data"
			}
			contentType := getContentType(requestedFile)
			StatusOK(writer, contentType, []byte(info))
		}
		writer.Flush()
		fmt.Println("Connection closed.")
		return
	} else if strings.ToUpper(firstHeaderFragment[0]) == "POST" {
		requestedFile := firstHeaderFragment[1]

		contentType := getContentType(requestedFile)
		fmt.Println(body)

		var content []byte
		if firstHeaderFragment[1] == "/hello-world" {
			content = getFileContent(getFileName(requestedFile), parseBody(body))
			created(writer, contentType, content)
		} else if firstHeaderFragment[1] == "/api/hello" {
			var requestBody RequestObj
			err := json.Unmarshal([]byte(body), &requestBody)
			if err != nil {
				fmt.Println(err)
			} else if requestBody.Request == "" {
				SendError(writer, "'request is a required property'", 400, "Bad Request")
			} else {
				response, err := http.Get("http://172.22.0.222:5000")
				if err != nil {
					fmt.Println(err)
				} else {
					resp := "Good "
					defer response.Body.Close()
					contents, _ := ioutil.ReadAll(response.Body)
					var contentsMap map[string]interface{}
					_ = json.Unmarshal(contents, &contentsMap)
					resp += fmt.Sprint(contentsMap["state"]) + ", "
					resp += requestBody.Request
					SendResponse(writer, resp, fmt.Sprint(currentVisit.Format(time.RFC3339)))
				}
			}

		} else {
			notFound(writer, "end point not found")
		}
		writer.Flush()
		fmt.Println("Connection closed.")
		return
	} else {
		content := "501 Not Implemented: Reason: " + firstHeaderFragment[0]
		notImplemented(writer, content)
		writer.Flush()
		fmt.Println("Connection closed.")
		return
	}

}

func getContentType(ends string) string {
	if strings.HasSuffix(ends, "/hello-world") {
		return "text/html"
	} else if strings.HasSuffix(ends, "/style") {
		return "text/css"
	} else if strings.HasSuffix(ends, "/background") {
		return "image/jpeg"
	} else if strings.HasSuffix(ends, ".yaml") {
		return "application/x-yaml"
	} else {
		return "text/plain"
	}
}

func getFileName(ends string) string {
	if strings.HasSuffix(ends, "/hello-world") {
		return ends + ".html"
	} else if strings.HasSuffix(ends, "/style") {
		return ends + ".css"
	} else if strings.HasSuffix(ends, "/background") {
		return ends + ".jpg"
	} else if strings.HasPrefix(ends, "/api/") {
		return strings.TrimPrefix(ends, "/api")
	} else {
		return ends + ".txt"
	}
}

func getFileContent(fileName, name string) []byte {
	b, err := ioutil.ReadFile("." + fileName)
	contentInString := string(b)
	if name != "" {
		contentInString = strings.Replace(contentInString, "__HELLO__", name, -1)
	}
	b = []byte(contentInString)
	if err != nil {
		fmt.Println(err.Error())
	}
	return b
}

func writeFileContent(fileName, content string) {
	ioutil.WriteFile("."+fileName, []byte(content), 0644)
}

func parseBody(body string) string {
	decodeUrl, err := url.QueryUnescape(strings.Split(body, "name=")[1])
	if err != nil {
		fmt.Println(err.Error())
		return ""
	} else {
		return decodeUrl
	}
}

func badRequest(writer *bufio.Writer, paramError string) {
	writer.WriteString("HTTP/1.1 400 Bad Request\n")
	writer.WriteString("Connection: close\n")
	writer.WriteString("Content-Type: text/plain; charset=UTF-8\n")
	writer.WriteString("Content-Length: " + strconv.Itoa(25+len(paramError)) + "\n\n")
	writer.WriteString("400 Bad Request: Reason: " + paramError)
}

func notFound(writer *bufio.Writer, paramError string) {
	writer.WriteString("HTTP/1.1 404 Not Found\n")
	writer.WriteString("Connection: close\n")
	writer.WriteString("Content-Type: text/plain; charset=UTF-8\n")
	writer.WriteString("Content-Length: " + strconv.Itoa(23+len(paramError)) + "\n\n")
	writer.WriteString("404 Not Found: Reason: " + paramError)
}

func notImplemented(writer *bufio.Writer, paramError string) {
	writer.WriteString("HTTP/1.1 501 Not Implemented\n")
	writer.WriteString("Connection: close\n")
	writer.WriteString("Content-Type: text/plain; charset=UTF-8\n")
	writer.WriteString("Content-Length: " + strconv.Itoa(29+len(paramError)) + "\n\n")
	writer.WriteString("501 Not Implemented: Reason: " + paramError)
}

func StatusOK(writer *bufio.Writer, paramType string, content []byte) {
	writer.WriteString("HTTP/1.1 200 OK\n")
	writer.WriteString("Connection: close\n")
	writer.WriteString("Content-Type: " + paramType + "; charset=UTF-8\n")
	writer.WriteString("Content-Length: " + strconv.Itoa(len(content)) + "\n\n")
	writer.Write(content)
}

func created(writer *bufio.Writer, paramType string, content []byte) {
	writer.WriteString("HTTP/1.1 201 Created\n")
	writer.WriteString("Connection: close\n")
	writer.WriteString("Content-Type: " + paramType + "; charset=UTF-8\n")
	writer.WriteString("Content-Length: " + strconv.Itoa(len(content)) + "\n\n")
	writer.Write(content)
}

func found(writer *bufio.Writer, param string) {
	writer.WriteString("HTTP/1.1 302 Found\n")
	writer.WriteString("Connection: close\n")
	writer.WriteString("Location: " + param + "\n")
	writer.WriteString("Content-Type: text/plain; charset=UTF-8\n")
	writer.WriteString("Content-Length: " + strconv.Itoa((21 + len(param))) + "\n\n")
	writer.WriteString("302 Found: Location: " + param)
}

type RequestObj struct {
	Request string `json:"request"`
}

type ResponseObj struct {
	Apiversion   string `json:"apiversion"`
	Count        int
	Currentvisit string
	Response     string `json:"response"`
}

type PlusoneretObj struct {
	Apiversion string `json:"apiversion"`
	Plusoneret int32  `json:"plusoneret"`
}

type ErrorObject struct {
	Detail string `json:"detail"`
	Status int    `json:"status"`
	Title  string `json:"title"`
}

type Yaml struct {
	Info struct {
		Version string `yaml:"version"`
	}
}

func NewRequest(request string) *RequestObj {
	return &RequestObj{
		Request: request,
	}
}

func NewResponse(response string) *ResponseObj {
	return &ResponseObj{
		Response: response,
	}
}
func NewPlusoneret(plusoneret int) *PlusoneretObj {
	return &PlusoneretObj{
		Plusoneret: int32(plusoneret),
	}
}

func NewError(detail string, status int, title string) *ErrorObject {
	return &ErrorObject{
		Detail: detail,
		Status: status,
		Title:  title,
	}
}

func SendResponse(writer *bufio.Writer, response string, currentvisit string) {
	yamlObj := Yaml{}

	err := yaml.Unmarshal([]byte(getFileContent("/spesifikasi.yaml", "")), &yamlObj)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	resp := NewResponse(response)
	resp.Apiversion = yamlObj.Info.Version
	resp.Count = updateValue()
	resp.Currentvisit = currentvisit
	writer.WriteString("HTTP/1.1 200 OK\n")
	writer.WriteString("Connection: close\n")
	writer.WriteString("Content-Type: application/json; charset=UTF-8\n\n")
	writer.WriteString(toJSON(resp))
}

func SendPlusoneret(writer *bufio.Writer, requestedVal int) {
	yamlObj := Yaml{}

	err := yaml.Unmarshal([]byte(getFileContent("/spesifikasi.yaml", "")), &yamlObj)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	plusoneret := NewPlusoneret(requestedVal + 1)
	plusoneret.Apiversion = yamlObj.Info.Version

	fmt.Println(plusoneret.Apiversion + " " + fmt.Sprint(plusoneret.Plusoneret))
	writer.WriteString("HTTP/1.1 200 OK\n")
	writer.WriteString("Connection: close\n")
	writer.WriteString("Content-Type: application/json; charset=UTF-8\n\n")
	writer.WriteString(toJSON(plusoneret))
}

func SendError(writer *bufio.Writer, detail string, status int, title string) {
	customError := NewError(detail, status, title)
	writer.WriteString("HTTP/1.1 " + fmt.Sprint(status) + " " + title + "\n")
	writer.WriteString("Connection: close\n")
	writer.WriteString("Content-Type: application/json; charset=UTF-8\n\n")
	writer.WriteString(toJSON(customError))
}

func toJSON(obj interface{}) string {
	b, err := json.Marshal(obj)
	if err != nil {
		fmt.Println(err)
		return ""
	}
	return string(b)
}

func updateValue() int {
	contents := getFileContent("./value.json", "")
	var contentsMap map[string]int

	_ = json.Unmarshal(contents, &contentsMap)
	contentsMap["count"] = contentsMap["count"] + 1
	writeFileContent("./value.json", toJSON(contentsMap))

	return contentsMap["count"]
}
